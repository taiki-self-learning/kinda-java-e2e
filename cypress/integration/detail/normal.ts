import LoginPage from '../../pages/login'
import HomePage from '../../pages/home'
import DetailPage from '../../pages/detail'

describe('detail画面(正常系)', () => {
    beforeEach(() => {
        LoginPage.visit()
        cy.url().should('include', '/login')
    })
    it('ユーザの権限が管理者権限の場合、管理画面ボタンが存在すること', () => {
        LoginPage.tranHomePageWithAdmin()
        cy.url().should('include', '/home')
        HomePage.tranDetailPage()
        cy.url().should('include', '/detail')
        DetailPage.tranAdminBtn.should('exist')
    })
    it('ユーザの権限がユーザ権限の場合、管理画面ボタンが存在しないこと', () => {
        LoginPage.tranHomePageWithUser()
        cy.url().should('include', '/home')
        HomePage.tranDetailPage()
        cy.url().should('include', '/detail')
        DetailPage.tranAdminBtn.should('not.exist')
    })
})
