import LoginPage from '../../pages/login'
import HomePage from '../../pages/home'

describe('home画面(正常系)', () => {
    beforeEach(() => {
        LoginPage.visit()
        cy.url().should('include', '/login')
    })
    it('ユーザの権限が管理者権限の場合、ユーザ名と権限が正しく表示されること', () => {
        LoginPage.tranHomePageWithAdmin()
        cy.url().should('include', '/home')
        HomePage.userName.should('have.text', 'admin')
        HomePage.userRole.should('have.text', '[ROLE_ADMIN]')
    })
    it('ユーザの権限がユーザ権限の場合、ユーザ名と権限が正しく表示されること', () => {
        LoginPage.tranHomePageWithUser()
        cy.url().should('include', '/home')
        HomePage.userName.should('have.text', 'user')
        HomePage.userRole.should('have.text', '[ROLE_USER]')
    })
})
