export default class AdminPage {
    static visit() {
        cy.visit('admin')
    }

    static logout() {
        cy.get('body > div > form:nth-child(2) > button').click()
    }

    static get userDetaTable() {
        return cy.get('body > div > table')
    }

    static inputId(id: string) {
        cy.get('#id').type(id)
    }

    static inputUserName(username: string) {
        cy.get('#username').type(username)
    }

    static inputPassWord(password: string) {
        cy.get('#password').type(password)
    }

    static inputEmail(email: string) {
        cy.get('#email').type(email)
    }

    static selectRole(authority: string) {
        cy.get('#authority').select(authority)
    }

    static tranResultRegist() {
        cy.get('body > div > form:nth-child(7) > button').click()
    }

    static inputIdDelete(id: string) {
        cy.get('#deleteId').type(id)
    }

    static tranResultDelete() {
        cy.get('body > div > form:nth-child(10) > button').click()
    }
}
