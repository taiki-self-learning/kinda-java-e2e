export default class HomePage {
    static visit() {
        cy.visit('home')
    }

    static get userName() {
        return cy.get('body > div > span:nth-child(3)')
    }

    static get userRole() {
        return cy.get('body > div > span:nth-child(5)')
    }

    static tranDetailPage() {
        cy.get('body > div > form:nth-child(6) > button').click()
    }
}
