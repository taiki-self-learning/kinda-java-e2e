export default class DetailPage {
    static visit() {
        cy.visit('detail')
    }

    static get tranAdminBtn() {
        return cy.get('body > div > form:nth-child(4) > button')
    }

    static tranAdminPage() {
        cy.get('body > div > form:nth-child(4) > button').click()
    }
}
