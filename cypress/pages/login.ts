export default class LoginPage {
    static visit() {
        cy.visit('login')
    }

    static inputId(id: string) {
        cy.get('body > div > form > input[type=text]:nth-child(4)').type(id)
    }

    static inputPassword(pass: string) {
        cy.get('body > div > form > input[type=password]:nth-child(9)').type(pass)
    }

    static tranHomePage() {
        cy.get('body > div > form > button').click()
    }

    static tranHomePageWithAdmin() {
        this.inputId('999999')
        this.inputPassword('admin')
        this.tranHomePage()
    }

    static tranHomePageWithUser() {
        this.inputId('000001')
        this.inputPassword('user')
        this.tranHomePage()
    }

    static tranHomePageWithout() {
        this.inputId('000000')
        this.inputPassword('test')
        this.tranHomePage()
    }
}
