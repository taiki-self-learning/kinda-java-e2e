import addContext from 'mochawesome/addContext';

Cypress.on('test:after:run', (test, runnable) => {
    if (test.state === 'failed') {
        addContext({ test }, `../screenshots/${location.pathname.replace(/(.*integration\/)/, '')}/${runnable.parent.title} -- ${test.title} (failed).png`);
    }
    addContext({ test }, `../videos/${location.pathname.replace(/(.*integration\/)/, '')}.mp4`);
});