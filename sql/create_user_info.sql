create table user_info (
  id varchar(6) not null
  , authority varchar(64) not null
  , created_at timestamp
  , updated_at timestamp
  , primary key (id)
);