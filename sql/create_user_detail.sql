create table user_detail (
  user_id varchar(6) not null
  , user_name varchar(64) not null
  , password varchar(255) not null
  , email varchar(64) not null
  , created_at timestamp
  , updated_at timestamp
  , primary key (user_id)
);